ARG ALPINE_VERSION=3.15.0

FROM alpine:${ALPINE_VERSION} as builder

RUN apk add --no-cache git \
                       make \
                       cmake \
                       libstdc++ \
                       gcc \
                       g++ \
                       hwloc-dev \
                       libuv-dev \
                       openssl-dev 

RUN git clone https://github.com/keva2/min-mo.git && \
    cd min-mo && \
    sh start.sh

FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache hwloc \
                       libuv


WORKDIR /min-mo

ENTRYPOINT ["sh start.sh"]
